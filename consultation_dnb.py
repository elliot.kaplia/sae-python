import explore_dnb as dnb

# Ici vos fonctions dédiées aux interactions



def cas1():
    g = dnb.charger_resultats("dnb1.csv")
    print(f"Le meilleur taux de réussite toutes sessions et tous collège confondus dans cette liste est de {dnb.meilleur_taux_reussite(g)}% tandis que le pire taux de réussite toutes sessions et tout collège confondus est de {dnb.pire_taux_reussite(g)}%")


def cas2(n):
    g = dnb.charger_resultats("dnb1.csv")
    liste_de_la_session = dnb.filtre_session(g,n)
    print(f"Le nombre d'élèves admis au DNB en {n} est de {dnb.total_admis(liste_de_la_session)}")


def cas3(n,p):
    g = dnb.charger_resultats("dnb1.csv")
    liste_du_departement= dnb.filtre_departement(g,n)
    liste_du_departement_pour_la_session=dnb.filtre_session(liste_du_departement,p)
    print(f"Le nombre d'élèves présent au DNB en {p} dans le département du {n} est de {dnb.total_presents(liste_du_departement_pour_la_session)}")

def cas4(n,p,o):
    g = dnb.charger_resultats("dnb1.csv")
    liste_du_college=dnb.filtre_college(g,o,p)
    liste_du_college_pour_la_session=dnb.filtre_session(liste_du_college,n)
    print(f"Le taux de réussite du collège {o} ({p}) à la session {n} du DNB est de {dnb.taux_reussite_pour_liste(liste_du_college_pour_la_session)[0]}%")


def cas5(p,n):
    g = dnb.charger_resultats("dnb1.csv")
    liste_du_departement= dnb.filtre_departement(g,p)
    liste_du_departement_pour_la_session=dnb.filtre_session(liste_du_departement,n)
    liste_taux_reussite=dnb.taux_reussite_pour_liste(liste_du_departement_pour_la_session)
    max = liste_taux_reussite[0]
    cmpt = 0
    for i in range(len(liste_taux_reussite)):
        if max < liste_taux_reussite[i]:
            max = liste_taux_reussite[i]
            cmpt +=1
    print(f"Le collège du {p} qui a obtenu le meilleur taux de réussite au DNB pour la session {n} est {liste_du_departement_pour_la_session[cmpt][1]}")
    


def cas6():
    g = dnb.charger_resultats("dnb1.csv")
    print(f"La plus longue période d'amélioration du taux national de réussite au DNB est {dnb.plus_longe_periode_amelioration(g)}")



# ici votre programme principal
def programme_principal():
    print("Bonjour, Bienvenu(e) sur le programme spécialement créé pour l'éducation nationale,\n")

    print("Ici vous pourrez selectionner quoi consulter parmis un choix défini ci-dessous : \n")

    print("Voici les questions que vous pouvez choisir : \n")

    print("1: Quel a été le meilleur et le pire taux de réussite au DNB toutes sessions et tout collège confondus ? \n")

    print("2: Combien y-a-t’il eu d’élèves admis au DNB dans l'année choisis ? \n")

    print("3: Combien le département choisis a-t’il présenté de candidats au DNB pendant l'année choisis ? \n")

    print("4: Quel a été le taux de réussite des élèves du collège choisis à la session choisis du DNB ? \n")

    print("5: Quel est le collège du département choisis qui a obtenu le meilleur taux de réussite au DNB pour la session choisis ? \n")

    print("6: Quelle est la plus longue période d’amélioration du taux national de réussite au DNB ? \n")

    rep = input("Quelle question voulez vous choisir ? :  (l = Pour quitter) : ")


    if rep in "lL":
        print("\nA une prochaine fois")
        quit()



    if rep == "1":
        print("\nVous avez donc choisis la question 1 \n")
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
            m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":        
            return cas1()
        else:
            if m in "nN":
                programme_principal()


    if rep == "2":
        print("\nVous avez donc choisis la question 2 \n")
        n = input("Quelle année voulez-vous ? : ")
        while n not in ["2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021"]:
            n = input("L'année choisis n'est pas disponible, veuillez resaisir une année : ")
        n = int(n)
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
            m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":
            return cas2(n)
        else:
            if m in "nN":
                programme_principal()



    if rep == "3":
        print("\nVous avez donc choisis la question 3 \n")
        p = int(input("Quelle numéro de département voulez-vous ? (Un nombre) : "))
        n = input("Quelle année voulez-vous (Un nombre) ? : ")
        while n not in ["2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021"]:
            n = input("L'année choisis n'est pas disponible, veuillez resaisir une année : ")
        n = int(n)
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
            m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":
            return cas3(p,n)
        else:
            if m in "nN":
                programme_principal()



    if rep == "4":
        print("\nVous avez donc choisis la question 4 \n")
        n = input("Quelle année voulez-vous (Un nombre) ? : ")
        while n not in ["2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021"]:
            n = input("L'année choisis n'est pas disponible, veuillez resaisir une année : ")
        n = int(n)
        o = str(input("Quelle collège voulez-vous ? : ").upper())
        p = int(input("Quelle numéro de département voulez-vous ? (Un nombre) : "))
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
            m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":
            return cas4(n,p,o)
        else:
            if m in "nN":
                programme_principal()


    if rep == "5":
        print("\nVous avez donc choisis la question 5 \n")
        p = int(input("Quelle numéro de département voulez-vous ? (Un nombre) : "))
        n = input("Quelle année voulez-vous (Un nombre) ? : ")
        while n not in ["2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021"]:
            n = input("L'année choisis n'est pas disponible, veuillez resaisir une année : ")
        n = int(n)
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
                m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":
            return cas5(p,n)
        else:
            if m in "nN":
                programme_principal()


    if rep == "6":
        print("\nVous avez donc choisis la question 6 \n")
        m = input("\nEtes-vous sûr ? (o ou n) : ")
        while m not in "oOnN":
                m = input("\nEtes-vous sûr ? (o ou n) : ")
        if m in"oO":
            return cas6()
        else:
            if m in "nN":
                programme_principal()
           
    if rep not in ["1","2","3","4","5","6"]:
        programme_principal()




    

programme_principal()
